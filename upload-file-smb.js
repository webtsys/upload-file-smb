
module.exports = function(RED) {
    function UploadFileSmb(config) {
        
        RED.nodes.createNode(this,config);
        
        var node = this;
        
        node.on('input', function(msg) {
            
            //console.log(msg.payload);
    
            /*folder: '//10.0.34.10/LOGS',
              username: 'developer',
              password: 'RpA&2020',
              domain: '10.0.34.10',*/
            
            const SambaClient = require('samba-client');
            
            if(msg.payload.hasOwnProperty('source') && msg.payload.hasOwnProperty('destiny')) {
            
                filename_source=msg.payload['source'];
                filename_destiny=msg.payload['destiny'];
                
            }
            else {
                
                filename_source=config['source'];
                filename_destiny=config['destiny'];
                
            }
            
            folder=config.folder;
            username=config.username;
            password=config.password;
            domain=config.domain;

            let client = new SambaClient({
              
              address: folder, // required
              username: username, // not required, defaults to guest
              password: password, // not required
              domain: domain, // not required
              maxProtocol: 'SMB3' // not required
            });

            /*var dt = new Date();

            var dformat=dt.getFullYear().toString().padStart(4, '0')
            +(dt.getMonth()+1).toString().padStart(2, '0')
            +dt.getDate().toString().padStart(2, '0')
            +dt.getHours().toString().padStart(2, '0')
            +dt.getMinutes().toString().padStart(2, '0')
            +dt.getSeconds().toString().padStart(2, '0');*/

            // send a file
            rv=client.sendFile(filename_source, filename_destiny);

            rv.then(function(value) {
                
                //Success
                msg.payload=true;
                node.send(msg);
                
            }, function(reason) {
                //Fail
                
                var err=reason;
                
                msg.payload=false;
                node.error(err, msg);
                
            });

            msg.payload;
            
        });
    }
    RED.nodes.registerType("upload-file-smb", UploadFileSmb);
}

